﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class ForgotPasswordViewModel
    {
        public string NLKId { get; set; }
        [Required(ErrorMessage ="The Password is Required!")]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string NewPasswordConfirm { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}