﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class Transaction
    {
        public DateTime VoucherDate { get; set; }
        public string FormattedDate { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Amount { get; set; }
        public string Days { get; set; }
        public string Interest { get; set; }
        public string Sum { get; set; }
        public string YearlySum { get; set; }
        public string YearlyIntrest { get; set; }
        public string yearid { get; set; }
        public string EmployeeCode { get; set; }

        public string TotalYearlyAmount { get; set; }
        public string TotalYearlyIntrest { get; set; }
        public string OfficeCode { get; set; }
        public string month_No { get; set; }
    }
}