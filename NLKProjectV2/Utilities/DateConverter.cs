﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities
{
    public class DateConverter
    {
        static int[] numbers = new int[] 
        { 2000, 
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31, 
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                30, 32, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                30, 32, 31, 32, 31, 31, 29, 30, 29, 30, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31,
                31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31,
                31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30,
                31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30,
                31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30,
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30,
                31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30,
                31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30,
                31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30,
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30,
                31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30,
                30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30,
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30,
                30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30   
            };

        public string ADtoBS(DateTime dtAD)
        {

            //DateTime dtAD = new DateTime();
            //dtAD = DateTime.Now;
            string weekday = dtAD.DayOfWeek.ToString();

            string nepaliWeekday;
            if (weekday == "Sunday")
                nepaliWeekday = "??????";
            else if (weekday == "Monday")
                nepaliWeekday = "??????";
            else if (weekday == "Tuesday")
                nepaliWeekday = "???????";
            else if (weekday == "Wednesday")
                nepaliWeekday = "??????";
            else if (weekday == "Thursday")
                nepaliWeekday = "???????";
            else if (weekday == "Friday")
                nepaliWeekday = "????????";
            else
                nepaliWeekday = "??????";


            int totalNepDaysCount = 0;
            int refNepYear = 2000;

            int refEngYear = 1943;
            int refEngMonth = 04;
            int refEngDay = 14;

            DateTime refEngDate = new DateTime(refEngYear, refEngMonth, refEngDay, 0, 0, 0);
            TimeSpan t = (dtAD - refEngDate);
            int totalEnglishDaysCount = t.Days;

            int count = 1;

            do
            {
                totalNepDaysCount = totalNepDaysCount + (numbers[count]);
                if (totalNepDaysCount > totalEnglishDaysCount) break;
                count++;

            } while (true);

            int nepYearCount = (count - 1) / 12;
            int nepaliYear = refNepYear + nepYearCount;

            //to use in unicode use nepaliYearFinal
            string nepaliYearFinal;
            if (nepaliYear == 2069)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2070)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2071)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2072)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2073)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2074)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2075)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2076)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2077)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2078)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2079)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2080)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2081)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2082)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2083)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2084)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2085)
                nepaliYearFinal = "????";
            else if (nepaliYear == 2086)
                nepaliYearFinal = "????";
            else
                nepaliYearFinal = "?";

            int nepMonth = count % 12;
            if (nepMonth == 0)
                nepMonth = 12;
            int nepDays = 0;
            for (int j = 1; j < count; j++)
            {
                nepDays += numbers[j];
            }
            double remNepDays = (totalEnglishDaysCount + 1) - nepDays;

            //To use in unicode try nepaliDayFinal
            string nepaliDayFinal;
            if (remNepDays == 1)
                nepaliDayFinal = "??";
            else if (remNepDays == 2)
                nepaliDayFinal = "??";
            else if (remNepDays == 3)
                nepaliDayFinal = "??";
            else if (remNepDays == 4)
                nepaliDayFinal = "??";
            else if (remNepDays == 5)
                nepaliDayFinal = "??";
            else if (remNepDays == 6)
                nepaliDayFinal = "??";
            else if (remNepDays == 7)
                nepaliDayFinal = "??";
            else if (remNepDays == 8)
                nepaliDayFinal = "??";
            else if (remNepDays == 9)
                nepaliDayFinal = "??";
            else if (remNepDays == 10)
                nepaliDayFinal = "??";
            else if (remNepDays == 11)
                nepaliDayFinal = "??";
            else if (remNepDays == 12)
                nepaliDayFinal = "??";
            else if (remNepDays == 13)
                nepaliDayFinal = "??";
            else if (remNepDays == 14)
                nepaliDayFinal = "??";
            else if (remNepDays == 15)
                nepaliDayFinal = "??";
            else if (remNepDays == 16)
                nepaliDayFinal = "??";
            else if (remNepDays == 17)
                nepaliDayFinal = "??";
            else if (remNepDays == 18)
                nepaliDayFinal = "??";
            else if (remNepDays == 19)
                nepaliDayFinal = "??";
            else if (remNepDays == 20)
                nepaliDayFinal = "??";
            else if (remNepDays == 21)
                nepaliDayFinal = "??";
            else if (remNepDays == 22)
                nepaliDayFinal = "??";
            else if (remNepDays == 23)
                nepaliDayFinal = "??";
            else if (remNepDays == 24)
                nepaliDayFinal = "??";
            else if (remNepDays == 25)
                nepaliDayFinal = "??";
            else if (remNepDays == 26)
                nepaliDayFinal = "??";
            else if (remNepDays == 27)
                nepaliDayFinal = "??";
            else if (remNepDays == 28)
                nepaliDayFinal = "??";
            else if (remNepDays == 29)
                nepaliDayFinal = "??";
            else if (remNepDays == 30)
                nepaliDayFinal = "??";
            else if (remNepDays == 31)
                nepaliDayFinal = "??";
            else if (remNepDays == 32)
                nepaliDayFinal = "??";
            else
                nepaliDayFinal = "?";

            //To use month in unicode use monthinbs
            string monthinbs;
            if (nepMonth == 1)
                monthinbs = "?????";
            else if (nepMonth == 2)
                monthinbs = "?????";
            else if (nepMonth == 3)
                monthinbs = "????";
            else if (nepMonth == 4)
                monthinbs = "??????";
            else if (nepMonth == 5)
                monthinbs = "?????";
            else if (nepMonth == 6)
                monthinbs = "????";
            else if (nepMonth == 7)
                monthinbs = "???????";
            else if (nepMonth == 8)
                monthinbs = "?????";
            else if (nepMonth == 9)
                monthinbs = "???";
            else if (nepMonth == 10)
                monthinbs = "???";
            else if (nepMonth == 11)
                monthinbs = "?????";
            else
                monthinbs = "?????";


            //Final Date in Unicode
            string dateInBSUnicode = nepaliYearFinal + " " + monthinbs + " " + nepaliDayFinal + ", " + nepaliWeekday;

            //Final Date in formal english language
            string dateInBSFormal = nepaliYear.ToString() + "-" + nepMonth.ToString() + "-" + remNepDays.ToString();
            //DateTime returnDate = new DateTime(Convert.ToInt32(nepaliYear), Convert.ToInt32(nepMonth), Convert.ToInt32(remNepDays));
            return dateInBSFormal;
        }

        public DateTime BStoAD(string dtBS)
        {

            long totalNepDaysCount = 0;
            int refNepYear = 2000;

            int refEngYear = 1943;
            int refEngMonth = 04;
            int refEngDay = 14;
            string[] BS = dtBS.Split('-');


            int nepYear = Convert.ToInt32(BS[0]);
            int nepMonth = Convert.ToInt32(BS[1]);
            int nepDay = Convert.ToInt32(BS[2]);

            int diffYear = nepYear - refNepYear;
            int diffMonths = diffYear * 12 + (nepMonth - 1);
            int diffDays = nepDay - 1;
            int daystoAdd = 0;

            for (int i = 1; i <= diffMonths; i++)
            {
                daystoAdd += numbers[i];
            }
            totalNepDaysCount = daystoAdd + diffDays;

            DateTime refEngDate = new DateTime(refEngYear, refEngMonth, refEngDay, 0, 0, 0);
            DateTime AddDate = new DateTime(refEngDate.Year, refEngDate.Month, refEngDate.Day);
            AddDate = AddDate.AddDays(totalNepDaysCount);
            return AddDate;
        }
    }
}
