﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class RegistrationModel
    {
        [Required(ErrorMessage="The NLK Id is Required")]
        public string NLKNumber { get; set; }
        [Required(ErrorMessage="The Password is required.")]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage="The email is required."), EmailAddress(ErrorMessage="The e-mail you entered is not valid.")]
        public string Email { get; set; }
        [Required(ErrorMessage="NLK ID Front Image is required")]
        public HttpPostedFileBase NlkIdImageFront { get; set; }
        public HttpPostedFileBase NlkIdImageBack { get; set; }
        public string imFrontName { get; set; }
        public string imBackName { get; set; }
        public string PassDate { get; set; }
        public int LoginFail { get; set; }
        public double Time { get; set; }
        public string DefaultPass { get; set; }
        public string State { get; set; }
        public string Validate { get; set; }
        public string MobileNo { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string GrandfatherName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string CitizenshipNumber { get; set; }
        public string CitizenshipIssueDistrict { get; set; }
        public int CitizenshipIssueDistrictId { get; set; }
        public string Gender { get; set; }
        public int PradeshId { get; set; }

    }
}