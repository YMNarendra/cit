﻿using NLKProjectV2.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace NLKProjectV2.Controllers
{
    public class OfficeController : Controller
    {
        private AuthDbContext _context;
        
        public OfficeController()
        {
            _context = new AuthDbContext();
        }
        // GET: Office
        public ActionResult Index()
        {
            ViewBag.Title = "Home | User Detail";
            var identity = (ClaimsIdentity)User.Identity;
            var officeID = identity.FindFirst("OfficeID").Value;

            var userList = _context.GetUsersByOfficeID(officeID);
            return View(userList);
        }

        [Route("office/useraccount/{id}")]
        public ActionResult UserAccount(string id)
        {
            ViewBag.Title = "Home" + " | " + "User Detail";
            var identity = (ClaimsIdentity)User.Identity;
            var officeID = identity.FindFirst("OfficeID").Value;
            //if (!_context.IsUserOffice(id,officeID))
            //{
            //    ViewBag.Status = _context.IsUserOffice(id, officeID);
            //    return View();
            //}
            int idint;
            if (int.TryParse(id, out idint))
            {
                try
                {
                    int userId = Convert.ToInt32(_context.GetIdByNLK(id));
                    ViewBag.data = _context.GetTransactionDetails1(Convert.ToInt32(userId), 0);
                    var transDetail = _context.GetTransactionDetails(Convert.ToInt32(userId), 0);
                    ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
                    ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
                    return View(transDetail);
                }
                catch (Exception Ex)
                {
                    ModelState.AddModelError("", Ex.Message);
                    return View();
                }
            }
            else
            {
                ModelState.AddModelError("", "The NLK ID for this user is not valid!");
                return View();
            }
        }
        [Route("office/useraccount/{id}")]
        [HttpPost]
        public ActionResult UserAccount(string id, int YearValue)
        {
            ViewBag.Title = "Home" + " | " + "User Detail";
            ViewBag.YearValue = YearValue;
            int idint;
            if (int.TryParse(id, out idint))
            {
                try
                {
                    int userId = Convert.ToInt32(_context.GetIdByNLK(id));
                    ViewBag.data = _context.GetTransactionDetails1(Convert.ToInt32(userId), YearValue);

                    var transDetail = _context.GetTransactionDetails(Convert.ToInt32(userId), YearValue);
                    //ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
                    //ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
                    return View(transDetail);
                }
                catch (Exception Ex)
                {
                    ModelState.AddModelError("", Ex.Message);
                    return View();
                }
            }
            else
            {
                ModelState.AddModelError("", "The NLK ID for this user is not valid!");
                return View();
            }
        }

       public JsonResult GetOfficeName(int id)
        {
           string lst = _context.GetOfficeNameByOfficeCode(id.ToString());
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public int GetOfficeNametEST(int id)
        {
            return id;
        }
    }
}