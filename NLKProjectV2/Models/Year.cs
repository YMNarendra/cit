﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class Year
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}