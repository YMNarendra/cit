﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class LogDetail
    {
        public string NLK_ID { get; set; }
        public DateTime LogDate { get; set; }
        public string LogDateNepali { get; set; }
        public string IP { get; set; }
    }
}