﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class Office
    {
        public int OfficeID { get; set; }
        public string OfficeName { get; set; }
        public string Address { get; set; }
        [Required(ErrorMessage ="CIT Office Code is required")]
        public int OfficeNLK { get; set; }
        public int DistrictId { get; set; }
        public int PradeshId { get; set; }
        [Required(ErrorMessage ="Application Letter is required.")]
        public HttpPostedFileBase ApplicationLetter { get; set; }
        public string ApplicationLetterPath { get; set; }
        public string Phone { get; set; }
        [Required(ErrorMessage = "The email is required."), EmailAddress(ErrorMessage = "The e-mail you entered is not valid.")]
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        [Required(ErrorMessage = "The Password is required.")]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmPassword { get; set; }
        public int IsVerified { get; set; }
        public string ApplicationDate { get; set; }
        public string DistrictName { get; set; }
        public string PradeshName { get; set; }
        public string VerifiedDate { get; set; }
    }
}