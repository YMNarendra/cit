﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class AdminPasswordChangeViewModel
    {
        [Required(ErrorMessage ="The Password is Required!")]
        public string NewPassword { get; set; }
        [Compare("NewPassword",ErrorMessage ="The Confirm Password must match the New Password")]
        public string NewPasswordConfirm { get; set; }
    }
}