﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class OfficeForgotPasswordModel
    {
        [Required(ErrorMessage = "NLK ID is required.")]
        public string OfficeID { get; set; }
        [EmailAddress(ErrorMessage = "Invalid e-mail address"), Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }
    }
}