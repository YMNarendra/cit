﻿using System.Collections.Generic;
using System.Web;

namespace NLKProjectV2.Models
{
    public class User
    {
        public string NLKNo { get; set; }
        public string DefaultPass { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string Ext { get; set; }
        public string Ext2 { get; set; }
        public string Validate { get; set; }
        public string CreatedDate { get; set; }
        public string NameOfUser { get; set; }
        public string VerifiedDate { get; set; }
        public int UserId { get; set; }
        public string MobileNo { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string GrandfatherName { get; set; }
        public string DateOfBirth { get; set; }
        public string CitizenshipNumber { get; set; }
        public string CitizenshipIssueDistrict { get; set; }
        public int CitizenshipIssueDistrictId { get; set; }
        public string Address { get; set; }
        public string OfficeName { get; set; }
        public string OfficeAddress { get; set; }
        public IEnumerable<LogDetail> LogDetails { get; set; }
        public string Gender { get; set; }
        public string GrandMotherName { get; set; }
        public string CitzDate { get; set; }
        public int PradeshId { get; set; }
        public int DistrictId { get; set; }
        public string SpouseName { get; set; }
        public string SpouseCitzNo { get; set; }
        public string SpouseCitzDate { get; set; }
        public string PAddress { get; set; }
        public string TAddress { get; set; }
        public string PradeshName { get; set; }
        public HttpPostedFileBase NlkIdImageFront { get; set; }
        public HttpPostedFileBase NlkIdImageBack { get; set; }
        public string imFrontName { get; set; }
        public string imBackName { get; set; }

        //For admin User
        public string Name { get; set; }
        public int IsActive { get; set; }
    }
}