﻿using NLKProjectV2.Models;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace NLKProjectV2.DbContexts
{
    public class AuthDbContext
    {
        private static string conn = ConfigurationManager.ConnectionStrings["OracleDbContext"].ConnectionString;
        private OracleConnection cnx = new OracleConnection(conn);

        public IEnumerable<User> GetUserList()
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETALLUSERS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new User
                {
                    NLKNo = dr["NLK_No"].ToString(),
                    DefaultPass = dr["Default_Pass"].ToString(),
                    State = dr["State"].ToString(),
                    CreatedDate = dr["Created_Date"].ToString(),
                    NameOfUser = dr["Name"].ToString(),
                    Email = dr["EMAIL"].ToString(),
                    MobileNo = dr["MOBILE_NUMBER"].ToString()
                };
            }
        }

        public IEnumerable<User> GetUserBySearchID(int id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETUSERBYID";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new User
                {
                    NLKNo = dr["NLK_No"].ToString(),
                    DefaultPass = dr["Default_Pass"].ToString(),
                    State = dr["State"].ToString(),
                    CreatedDate = dr["Created_Date"].ToString(),
                    NameOfUser = dr["Name"].ToString()
                };
            }
        }

        public User ValidateUser(int id, string password)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETUSERFORAUTHENTICATION";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("passwordy", OracleDbType.Varchar2, ParameterDirection.Input).Value = password;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    NLKNo = dt.Rows[0]["User"].ToString(),
                    Name = dt.Rows[0]["NAME"].ToString(),
                    UserId = Convert.ToInt32(dt.Rows[0]["ID"].ToString()),
                    IsActive = Convert.ToInt32(dt.Rows[0]["ACTIVE"].ToString())
                };
            }
            else
            {
                return new User { };
            }

        }

        public User ValidateAdmin(string name, string password)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETADMINUSER";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Varchar2, ParameterDirection.Input).Value = name;
            cmd.Parameters.Add("passwordy", OracleDbType.Varchar2, ParameterDirection.Input).Value = password;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    Name = dt.Rows[0]["User"].ToString()
                };
            }
            else
            {
                return new User { };
            }
        }

        public void AdminPasswordChange(string user_id, string password)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_UPDATEADMINPASSWORD";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("user_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = user_id;
            cmd.Parameters.Add("pass_word", OracleDbType.Varchar2, ParameterDirection.Input).Value = password;
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        public void DeleteVerifiedUser(string id)
        {
            string fullPath = "";
            string fullPath1 = "";

            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            IEnumerable<User> fileNames = getUserFileName(id);
            foreach (var file in fileNames)
            {
                fullPath = HttpContext.Current.Request.MapPath("~/Uploads/" + file.Ext);
                fullPath1 = HttpContext.Current.Request.MapPath("~/Uploads/" + file.Ext2);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                if (System.IO.File.Exists(fullPath1))
                {
                    System.IO.File.Delete(fullPath1);
                }
            }
            cmd.CommandText = "USP_DELETEVERIFIEDUSERS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("user_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;
            cnx.Open();
            cmd.ExecuteNonQuery();


            cnx.Close();
        }
        public void DeleteVerifiedOfice(string id)
        {
            string fullPath = "";
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            IEnumerable<Office> fileNames = getOfficeFileName(id);
            foreach (var file in fileNames)
            {
                fullPath = HttpContext.Current.Request.MapPath("~/Uploads/officeapplications/" + file.ApplicationLetterPath);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
            }
            cmd.CommandText = "DELETE FROM AUTH_OFFICE WHERE OfficeNLK = :office_Id";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("office_Id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();


        }
        public Office ValidateOffice(string name, string password)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETOFFICE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = name;
            cmd.Parameters.Add("off_password", OracleDbType.Varchar2, ParameterDirection.Input).Value = password;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new Office
                {
                    OfficeNLK = Convert.ToInt32(dt.Rows[0]["officenlk"].ToString()),
                    OfficeName = dt.Rows[0]["name"].ToString(),
                    Address = dt.Rows[0]["address"].ToString(),
                    DistrictName = dt.Rows[0]["districtname"].ToString()
                };
            }
            else
            {
                return new Office { };
            }

        }
        public RegistrationModel InsertUser(RegistrationModel newUser)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_INSERTUSER";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("inuser", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.NLKNumber;
            cmd.Parameters.Add("inpassword", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.Password;
            cmd.Parameters.Add("inpass_date", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.PassDate;
            cmd.Parameters.Add("inlogin_fail", OracleDbType.Int32, ParameterDirection.Input).Value = newUser.LoginFail;
            cmd.Parameters.Add("intime", OracleDbType.Int64, ParameterDirection.Input).Value = newUser.Time;
            cmd.Parameters.Add("indefault_pass", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.DefaultPass;
            cmd.Parameters.Add("instate", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.State;
            cmd.Parameters.Add("inemail", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.Email;
            cmd.Parameters.Add("invalidate_field", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.Validate;
            cmd.Parameters.Add("inphoto1", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.imFrontName;
            cmd.Parameters.Add("inphoto2", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.imBackName;
            cmd.Parameters.Add("mobile_no", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.MobileNo;
            cmd.Parameters.Add("father_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.FatherName;
            cmd.Parameters.Add("mother_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.MotherName;
            cmd.Parameters.Add("grandfather_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.GrandfatherName;
            cmd.Parameters.Add("dob", OracleDbType.Date, ParameterDirection.Input).Value = newUser.DateOfBirth;
            cmd.Parameters.Add("citizen_number", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.CitizenshipNumber;
            cmd.Parameters.Add("citizen_issue_district_id", OracleDbType.Int32, ParameterDirection.Input).Value = newUser.CitizenshipIssueDistrictId;
            cmd.Parameters.Add("gender", OracleDbType.Varchar2, ParameterDirection.Input).Value = newUser.Gender;
            cmd.Parameters.Add("pradesh_id", OracleDbType.Int32, ParameterDirection.Input).Value = newUser.PradeshId;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new RegistrationModel
                {
                    NLKNumber = dt.Rows[0]["User"].ToString()
                };
            }
            else
            {
                return new RegistrationModel { };
            }
        }

        public User GetUserById(int id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETUSERBYID";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    NLKNo = dt.Rows[0]["NLK_No"].ToString(),
                    DefaultPass = dt.Rows[0]["Default_pass"].ToString(),
                    State = dt.Rows[0]["State"].ToString(),
                    Email = dt.Rows[0]["Email"].ToString(),
                    Ext = dt.Rows[0]["Ext"].ToString(),
                    Ext2 = dt.Rows[0]["Ext2"].ToString(),
                    NameOfUser = dt.Rows[0]["Name"].ToString(),
                    CreatedDate = dt.Rows[0]["Created_Date"].ToString(),
                    MobileNo = dt.Rows[0]["MOBILE_NUMBER"].ToString(),
                    FatherName = dt.Rows[0]["FATHER_NAME"].ToString(),
                    MotherName = dt.Rows[0]["MOTHER_NAME"].ToString(),
                    GrandfatherName = dt.Rows[0]["GRANDFATHER_NAME"].ToString(),
                    //DateOfBirth = String.IsNullOrEmpty(dt.Rows[0]["DATE_OF_BIRTH"].ToString())? new DateTime("") : Convert.ToDateTime(dt.Rows[0]["DATE_OF_BIRTH"].ToString()),
                    CitizenshipNumber = dt.Rows[0]["CITIZEN_NUMBER"].ToString(),
                    CitizenshipIssueDistrict = dt.Rows[0]["DistrictNameNEPALI"].ToString(),
                    LogDetails = GetUserLogById(id.ToString()),
                    Gender = dt.Rows[0]["Gender"].ToString(),
                    GrandMotherName = dt.Rows[0]["GRANDMOTHER_NAME"].ToString(),
                    CitzDate = dt.Rows[0]["CIT_DATE"].ToString(),
                    CitizenshipIssueDistrictId = Convert.ToInt32(dt.Rows[0]["CITIZEN_ISSUE_DISTRICT_ID"].ToString()),
                    PradeshId = Convert.ToInt32(dt.Rows[0]["PRADESHID"].ToString()),
                    PradeshName = dt.Rows[0]["PRADESHNAME"].ToString(),
                    SpouseCitzDate = dt.Rows[0]["SPOUSE_CITZ_DATE"].ToString(),
                    SpouseCitzNo = dt.Rows[0]["SPOUSE_CITZ_NO"].ToString(),
                    SpouseName = dt.Rows[0]["SPOUSE_NAME"].ToString(),
                    PAddress = dt.Rows[0]["P_ADDRESS"].ToString(),
                    TAddress = dt.Rows[0]["T_ADDRESS"].ToString(),
                };
            }
            else
            {
                return new User { };
            }
        }

        public IEnumerable<LogDetail> GetUserLogById(string nlk)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETLOGDETAILS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlk_id", OracleDbType.Int32, ParameterDirection.Input).Value = nlk;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new LogDetail
                {
                    NLK_ID = dr["NLK_ID"].ToString(),
                    LogDate = Convert.ToDateTime(dr["LOG_DATE"].ToString()),
                    LogDateNepali = dr["LOG_DATE_NEP"].ToString(),
                    IP = dr["IP"].ToString()
                };
            }
        }
        public IEnumerable<User> getUserFileName(string id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "select ext,ext2 from AUTHENTICATION where \"USER\" = :user_id";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("user_id1", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new User
                {
                    Ext = dr["Ext"].ToString(),
                    Ext2 = dr["Ext2"].ToString()
                };

                //string ext = dt.Rows[0]["EXT"].ToString();
                //fullPath = HttpContext.Current.Request.MapPath("~/Uploads/" + ext);

                //string ext2 = dt.Rows[0]["EXT2"].ToString();
                //fullPath1 = HttpContext.Current.Request.MapPath("~/Uploads/" + ext2);

            }
            cnx.Close();
        }
        public IEnumerable<Office> getOfficeFileName(string id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "select APPLICATIONlETTERNAME from auth_office where OFFICENLK = :office_id";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new Office
                {
                    ApplicationLetterPath = dr["APPLICATIONlETTERNAME"].ToString(),

                };

                //string ext = dt.Rows[0]["EXT"].ToString();
                //fullPath = HttpContext.Current.Request.MapPath("~/Uploads/" + ext);

                //string ext2 = dt.Rows[0]["EXT2"].ToString();
                //fullPath1 = HttpContext.Current.Request.MapPath("~/Uploads/" + ext2);

            }
            cnx.Close();
        }
        public User VerifyUser(int id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_VERIFYUSER";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    NLKNo = dt.Rows[0]["NLK_No"].ToString(),
                    NameOfUser = dt.Rows[0]["Name"].ToString()
                };
            }
            else
            {
                return new User { };
            }
        }

        public User RejectUser(int id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_REJECTUSER";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    Ext = dt.Rows[0]["EXT"].ToString(),
                    Ext2 = dt.Rows[0]["EXT2"].ToString()

                };
            }
            else
            {
                return new User { };
            }
        }

        public IEnumerable<User> GetVerifiedUsersList()
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETVERIFIEDUSERSLIST";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new User
                {
                    NLKNo = dr["NLK_No"].ToString(),
                    DefaultPass = dr["Default_Pass"].ToString(),
                    State = dr["State"].ToString(),
                    CreatedDate = dr["Created_Date"].ToString(),
                    VerifiedDate = dr["verified_date"].ToString(),
                    NameOfUser = dr["Name"].ToString(),
                    Email = dr["EMAIL"].ToString(),
                    MobileNo = dr["MOBILE_NUMBER"].ToString()
                };
            }
        }

        public User ChangePassword(string name, string password, string newPassword)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_CHANGEUSERPASSWORD";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Varchar2, ParameterDirection.Input).Value = name;
            cmd.Parameters.Add("passwordy", OracleDbType.Varchar2, ParameterDirection.Input).Value = password;
            cmd.Parameters.Add("newPassword", OracleDbType.Varchar2, ParameterDirection.Input).Value = newPassword;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    Name = dt.Rows[0]["User"].ToString()
                };
            }
            else
            {
                return new User { };
            }
        }

        public User UserPasswordChange(string nlk, string password)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_CHANGEUSERPASSWORDREQUEST";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Varchar2, ParameterDirection.Input).Value = nlk;
            cmd.Parameters.Add("newPassword", OracleDbType.Varchar2, ParameterDirection.Input).Value = password;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    Name = dt.Rows[0]["User"].ToString()
                };
            }
            else
            {
                return new User { };
            }
        }


        //GET YEAR LIST
        public IEnumerable<Year> GetYearList()
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETYEARLIST";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new Year
                {
                    Id = Convert.ToInt32(dr["ID"].ToString()),
                    Value = dr["AY_DESC"].ToString()
                };
            }
        }

        public User GetUserDetails(int id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETUSERDETAILS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("emp_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    Name = dt.Rows[0]["Name"].ToString(),
                    NLKNo = dt.Rows[0]["NLK_NO"].ToString(),
                };
            }
            else
            {
                return new User { };
            }

        }

        public Office GetOfficeDetails(int id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETOFFICEDETAILS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("emp_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new Office
                {
                    OfficeID = Convert.ToInt32(dt.Rows[0]["ID"].ToString()),
                    OfficeName = dt.Rows[0]["Name"].ToString(),
                    Address = dt.Rows[0]["Address"].ToString(),
                };
            }
            else
            {
                return new Office { };
            }

        }

        public Office GetImmediateOfficeDetails(int id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETIMMEDIATEOFFICEBYEMPID";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("emp_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 1)
            {
                return new Office
                {
                    OfficeID = Convert.ToInt32(dt.Rows[1]["Office_ID"].ToString()),
                    OfficeName = dt.Rows[1]["Name"].ToString(),
                    //Address = dt.Rows[1]["Address"].ToString(),
                };
            }
            else
            {
                return new Office { };
            }

        }

        private string GetNepDate(string eng_date)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_RETURNNEPALIDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("english_date", OracleDbType.Varchar2, ParameterDirection.Input).Value = eng_date;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0]["NEP_DATE"].ToString();
        }

        private string GetMonthName(string monthID)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETMONTHNAME";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("english_date", OracleDbType.Int32, ParameterDirection.Input).Value = monthID;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0]["NAME"].ToString();
        }

        private IEnumerable<Transaction> GetTransactions(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                yield return new Transaction
                {
                    Month = GetMonthName(dr["TMONTH_ID"].ToString()),
                    VoucherDate = Convert.ToDateTime(dr["VDATE"].ToString()),
                    FormattedDate = GetNepDate(Convert.ToDateTime(dr["VDATE"].ToString()).ToString("d-MMM-yy")),
                    Year = dr["TYEAR"].ToString(),
                    Amount = dr["AMOUNT"].ToString(),
                    Days = dr["IDAYS"].ToString(),
                    Interest = dr["INTEREST"].ToString()
                };
            }
        }

        public TransactionDetailViewModel GetTransactionDetails(int id, int yearid)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            if (yearid == 0)
            {
                cmd.CommandText = "SELECT trans_d.*, trans_m.TYEAR, trans_m.TMONTH_ID FROM trans_d, trans_m WHERE trans_d.TRANS_ID = trans_m.ID AND trans_d.EMP_ID =" + id + " ORDER BY trans_m.TYEAR, VDATE ASC";
            }
            else
            {
                cmd.CommandText = "SELECT trans_d.*, trans_m.TYEAR, trans_m.TMONTH_ID FROM trans_d, trans_m WHERE trans_d.TRANS_ID = trans_m.ID AND trans_d.EMP_ID =" + id + " AND trans_d.YEAR_ID =" + yearid + " ORDER BY trans_m.TYEAR, VDATE ASC";
            }
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);



            return new TransactionDetailViewModel
            {
                YearList = GetYearList(),
                EmployeeDetail = GetUserDetails(id),
                OfficeDetail = GetOfficeDetails(id),
                ImmediateOffice = GetImmediateOfficeDetails(id),
                Transactions = GetTransactions(dt),
                Accounts = GetAccountDetails(id),
            };



        }
        public List<Transaction> GetTransactionDetails1(int id, int yearid)
        {
            List<Transaction> Record = new List<Transaction>();
            List<AccountModel> account = new List<AccountModel>();
            account = GetAccountDetails(id).ToList();
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            if (yearid == 0)
            {
                //cmd.CommandText = "SELECT trans_d.*, trans_m.TYEAR, trans_m.TMONTH_ID FROM trans_d, trans_m WHERE trans_d.TRANS_ID = trans_m.ID AND trans_d.EMP_ID =" + id + " ORDER BY trans_m.TYEAR, VDATE ASC";
                cmd.CommandText = "SELECT trans_d.*, trans_m.TYEAR, trans_m.TMONTH_ID, office.CODE FROM trans_d, trans_m, office WHERE trans_d.TRANS_ID = trans_m.ID AND trans_d.EMP_ID =" + id + " AND trans_d.OFFICE_ID = office.ID ORDER BY VDATE ASC";
            }
            else
            {
                //cmd.CommandText = "SELECT trans_d.*, trans_m.TYEAR, trans_m.TMONTH_ID FROM trans_d, trans_m WHERE trans_d.TRANS_ID = trans_m.ID AND trans_d.EMP_ID =" + id + " AND trans_d.YEAR_ID =" + yearid + " ORDER BY trans_m.TYEAR, VDATE ASC";
                cmd.CommandText = "SELECT trans_d.*, trans_m.TYEAR, trans_m.TMONTH_ID, office.CODE FROM trans_d, trans_m, office WHERE trans_d.TRANS_ID = trans_m.ID AND trans_d.EMP_ID =" + id + " AND trans_d.YEAR_ID =" + yearid + " AND trans_d.OFFICE_ID = office.ID ORDER BY VDATE ASC";
            }
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            da.Fill(ds);
            decimal Rakam = 0;
            decimal byaj = 0;
            decimal totalYearlyAmount = 0;
            decimal totalYearlyIntrest = 0;
            int yearId = 0;
            int secondYearId = 0;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                foreach (var test in account)
                {
                    if (dr[10].ToString() == test.YearID)
                    {
                        if (test.OpeningInterest == "")
                        {
                            test.OpeningInterest = "0";
                        }
                        if (test.OpeningBalance == "")
                        {
                            test.OpeningBalance = "0";
                        }
                        totalYearlyAmount = Convert.ToDecimal(test.OpeningBalance);
                        totalYearlyIntrest = Convert.ToDecimal(test.OpeningInterest);
                    }
                }
                yearId = Convert.ToInt32(dr[10].ToString());
                if (yearId == 0 || yearId == secondYearId)
                {
                    secondYearId = yearId;
                }
                else
                {

                    byaj = 0;
                    Rakam = 0;
                    secondYearId = yearId;
                }
                if (dr[6].ToString() == "")
                {
                    dr[6] = "0";
                }

                byaj = byaj + Convert.ToDecimal(dr[6]);
                Rakam = Rakam + Convert.ToDecimal(dr[2]);
                Record.Add(new Transaction() { VoucherDate = Convert.ToDateTime(dr[18].ToString()), FormattedDate = GetNepDate(Convert.ToDateTime(dr[18].ToString()).ToString("d-MMM-yyyy")), Year = dr[20].ToString(), Month = GetMonthName(dr[21].ToString()), Amount = dr[2].ToString(), Days = dr[5].ToString(), Interest = dr[6].ToString(), YearlySum = Rakam.ToString(), yearid = dr[10].ToString(), YearlyIntrest = byaj.ToString(), EmployeeCode = dr[15].ToString(), TotalYearlyAmount = totalYearlyAmount.ToString(), TotalYearlyIntrest = totalYearlyIntrest.ToString(), OfficeCode = dr["CODE"].ToString(), month_No = dr["TMONTH_ID"].ToString() });

            }

            //cmd.CommandType = CommandType.Text;
            //da.SelectCommand = cmd;
            //DataTable dt = new DataTable();
            //da.Fill(dt);



            //    return new TransactionDetailViewModel
            //    {
            //        YearList = GetYearList(),
            //        EmployeeDetail = GetUserDetails(id),
            //        OfficeDetail = GetOfficeDetails(id),
            //        ImmediateOffice = GetImmediateOfficeDetails(id),
            //        Transactions = GetTransactions(dt),
            //        Accounts = GetAccountDetails(id),
            //    };     

            return Record;

        }
        //public List<Transaction> GetTransactionDetail(int id, int yearid)
        //{
        //    List<Transaction> Record = new List<Transaction>();

        //    OracleDataAdapter da = new OracleDataAdapter();

        //    OracleCommand cmd = new OracleCommand();
        //    cmd.Connection = cnx;
        //    cmd.InitialLONGFetchSize = 1000;
        //    var yearList = GetYearList();
        //    foreach (var item in yearList)
        //    {

        //        cmd.CommandText = "SELECT trans_d.*, trans_m.TYEAR, trans_m.TMONTH_ID FROM trans_d, trans_m WHERE trans_d.TRANS_ID = trans_m.ID AND trans_d.EMP_ID =" + id + " AND trans_d.YEAR_ID =" + item.Id + " ORDER BY trans_m.TYEAR, VDATE ASC";
        //        cmd.CommandType = CommandType.Text;
        //        da.SelectCommand = cmd;
        //        DataSet ds = new DataSet();
        //        da.Fill(ds);
        //        decimal Rakam = 0;
        //        decimal byaj = 0;
        //        foreach (DataRow dr in ds.Tables[0].Rows)
        //        {
        //            if(dr[6].ToString()=="")
        //            {
        //                dr[6] = "0";
        //            }
        //            byaj = byaj + Convert.ToDecimal(dr[6]);
        //            Rakam = Rakam + Convert.ToDecimal(dr[2]);
        //            Record.Add(new Transaction() { VoucherDate = Convert.ToDateTime(dr[18].ToString()), FormattedDate = GetNepDate(Convert.ToDateTime(dr[18].ToString()).ToString("d-MMM-yy")), Year = dr[20].ToString(), Month = GetMonthName(dr[21].ToString()), Amount = dr[2].ToString(), Days = dr[5].ToString(), Interest = dr[6].ToString(),YearlySum = Rakam.ToString(),yearid = dr[10].ToString(),YearlyIntrest = byaj.ToString() });
        //        }
        //    }
        //    return Record;

        //}



        public User VerifyEmailAddress(string id, string email)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_VERIFYEMAILADDRESS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("email_in", OracleDbType.Varchar2, ParameterDirection.Input).Value = email;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new User
                {
                    NLKNo = dt.Rows[0]["User"].ToString(),
                    DefaultPass = dt.Rows[0]["Default_pass"].ToString(),
                    State = dt.Rows[0]["State"].ToString(),
                    Email = dt.Rows[0]["Email"].ToString(),
                    Ext = dt.Rows[0]["Ext"].ToString(),
                    Ext2 = dt.Rows[0]["Ext2"].ToString()
                };
            }
            else
            {
                return new User { };
            }
        }

        public Office VerifyEmailAddressOffice(string id, string email)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_VERIFYEMAILADDRESSOFFICE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("off_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("off_email", OracleDbType.Varchar2, ParameterDirection.Input).Value = email;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new Office
                {
                    Email = dt.Rows[0]["Email"].ToString(),
                    IsVerified = Convert.ToInt32(dt.Rows[0]["ISVERIFIED"].ToString())
                };
            }
            else
            {
                return new Office { };
            }
        }

        public void InsertPasswordInfo(string nlk, string uniqueID, int isPasswordChanged)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_INSERTFORGOTPASSWORD";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("inuser", OracleDbType.Varchar2, ParameterDirection.Input).Value = nlk;
            cmd.Parameters.Add("inpassword", OracleDbType.Varchar2, ParameterDirection.Input).Value = uniqueID;
            cmd.Parameters.Add("inpass_date", OracleDbType.Int32, ParameterDirection.Input).Value = isPasswordChanged;
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        public void InsertPasswordInfoOffice(string nlk, string uniqueID, int isPasswordChanged)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_INSERTFORGOTPASSWORDOFFICE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("off_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = nlk;
            cmd.Parameters.Add("unique_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = uniqueID;
            cmd.Parameters.Add("is_pwd_changed", OracleDbType.Int32, ParameterDirection.Input).Value = isPasswordChanged;
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public void InsertLogDetails(string nlk)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_INSERTAUTHLOG";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlk_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = nlk;
            cmd.Parameters.Add("user_ip", OracleDbType.Varchar2, ParameterDirection.Input).Value = GetIPAddress();
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        public IEnumerable<AccountModel> GetAccountDetails(int id)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_RETURNTOTAL";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new AccountModel
                {
                    //Id = Convert.ToInt32(dr["EMP_ID"].ToString()),
                    YearID = dr["YEAR_ID"].ToString(),
                    YearName = dr["AY_DESC"].ToString(),
                    OpeningBalance = dr["OBALANCE"].ToString(),
                    Deposit = (String.IsNullOrEmpty(dr["DEPOSIT"].ToString()) ? 0 : Convert.ToDecimal(dr["DEPOSIT"].ToString())),
                    Interest = dr["INTEREST"].ToString(),
                    OpeningInterest = dr["OINTEREST"].ToString(),
                    InterestRate = dr["IRATE"].ToString(),
                    YearDays = dr["IDAYS"].ToString()
                };
            }
        }

        public IEnumerable<User> GetUsersByOfficeID(string offID)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETUSERSBYOFFICEID";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("off_id", OracleDbType.Int32, ParameterDirection.Input).Value = offID;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new User
                {
                    Name = dr["name"].ToString(),
                    Address = dr["Address"].ToString(),
                    NLKNo = dr["NLK_No"].ToString(),
                    OfficeName = dr["OFFICE_NAME"].ToString(),
                    OfficeAddress = dr["OFFICE_ADDRESS"].ToString(),
                    MobileNo = dr["MOBILE_NUMBER"].ToString()
                };
            }
        }

        public IEnumerable<User> SearchUnverifiedUsers(int? id, string email, string mobile)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_SEARCHUNVERIFIEDUSERS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("off_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("e_mail", OracleDbType.Varchar2, ParameterDirection.Input).Value = email;
            cmd.Parameters.Add("mobile_no", OracleDbType.Varchar2, ParameterDirection.Input).Value = mobile;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new User
                {
                    NLKNo = dr["NLK_No"].ToString(),
                    DefaultPass = dr["Default_Pass"].ToString(),
                    State = dr["State"].ToString(),
                    CreatedDate = dr["Created_Date"].ToString(),
                    NameOfUser = dr["Name"].ToString(),
                    Email = dr["EMAIL"].ToString(),
                    MobileNo = dr["MOBILE_NUMBER"].ToString()
                };
            }
        }

        public IEnumerable<User> SearchVerifiedUsers(int? id, string email, string mobile)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_SEARCHVERIFIEDUSERS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("off_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("e_mail", OracleDbType.Varchar2, ParameterDirection.Input).Value = email;
            cmd.Parameters.Add("mobile_no", OracleDbType.Varchar2, ParameterDirection.Input).Value = mobile;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new User
                {
                    NLKNo = dr["NLK_No"].ToString(),
                    DefaultPass = dr["Default_Pass"].ToString(),
                    State = dr["State"].ToString(),
                    CreatedDate = dr["Created_Date"].ToString(),
                    NameOfUser = dr["Name"].ToString(),
                    Email = dr["EMAIL"].ToString(),
                    MobileNo = dr["MOBILE_NUMBER"].ToString()
                };
            }
        }
        public void UpdateUserInfoData(
            string nlk, string email, string mobile, string fatherName, string motherName, string grandfatherName,
            string grandmotherName, string gender, int citsIssueDistrictId, string citzDate, int pradeshId,
            string spouseName, string spouseCitzNo, string spouseCitzDate, string pAddress, string tAddress, string imFrontName, string imBackName
            )
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_UPDATEUSERINFO";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("user_nlk", OracleDbType.Varchar2, ParameterDirection.Input).Value = nlk;
            cmd.Parameters.Add("user_email", OracleDbType.Varchar2, ParameterDirection.Input).Value = email;
            cmd.Parameters.Add("user_mobile", OracleDbType.Varchar2, ParameterDirection.Input).Value = mobile;
            cmd.Parameters.Add("gender", OracleDbType.Varchar2, ParameterDirection.Input).Value = gender;
            cmd.Parameters.Add("father_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = fatherName;
            cmd.Parameters.Add("mother_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = motherName;
            cmd.Parameters.Add("grandfather_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = grandfatherName;
            cmd.Parameters.Add("grandmother_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = grandmotherName;
            cmd.Parameters.Add("citz_issue_dis_id", OracleDbType.Int32, ParameterDirection.Input).Value = citsIssueDistrictId;
            cmd.Parameters.Add("citz_date", OracleDbType.Varchar2, ParameterDirection.Input).Value = citzDate;
            cmd.Parameters.Add("pradesh_id", OracleDbType.Int32, ParameterDirection.Input).Value = pradeshId;
            cmd.Parameters.Add("spouse_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = spouseName;
            cmd.Parameters.Add("spouse_citz_no", OracleDbType.Varchar2, ParameterDirection.Input).Value = spouseCitzNo;
            cmd.Parameters.Add("spouse_citz_date", OracleDbType.Varchar2, ParameterDirection.Input).Value = spouseCitzDate;
            cmd.Parameters.Add("p_address", OracleDbType.Varchar2, ParameterDirection.Input).Value = pAddress;
            cmd.Parameters.Add("t_address", OracleDbType.Varchar2, ParameterDirection.Input).Value = tAddress;
            cmd.Parameters.Add("inphoto1", OracleDbType.Varchar2, ParameterDirection.Input).Value = imFrontName;
            cmd.Parameters.Add("inphoto2", OracleDbType.Varchar2, ParameterDirection.Input).Value = imBackName;
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
        }
        public IEnumerable<District> GetDistrictList()
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETDISTRICTLIST";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new District
                {
                    Id = Convert.ToInt32(dr["DistrictId"].ToString()),
                    Name = dr["DistrictName"].ToString(),
                    NepaliName = dr["DistrictNameNepali"].ToString()
                };
            }
        }
        public IEnumerable<Pradesh> GetPradeshList()
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETPRADESHLIST";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new Pradesh
                {
                    Id = Convert.ToInt32(dr["PradeshId"].ToString()),
                    Name = dr["Name"].ToString(),
                };
            }
        }

        //Office FUNCTIONS
        public Office InsertOffice(Office office)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_INSERTOFFICE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlk_id", OracleDbType.Int32, ParameterDirection.Input).Value = office.OfficeNLK;
            cmd.Parameters.Add("off_add", OracleDbType.Varchar2, ParameterDirection.Input).Value = office.Address;
            cmd.Parameters.Add("dis_id", OracleDbType.Int32, ParameterDirection.Input).Value = office.DistrictId;
            cmd.Parameters.Add("pradesh_id", OracleDbType.Int32, ParameterDirection.Input).Value = office.PradeshId;
            cmd.Parameters.Add("application_path", OracleDbType.Varchar2, ParameterDirection.Input).Value = office.ApplicationLetterPath;
            cmd.Parameters.Add("phone_num", OracleDbType.Varchar2, ParameterDirection.Input).Value = office.Phone;
            cmd.Parameters.Add("email_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = office.Email;
            cmd.Parameters.Add("contact_person", OracleDbType.Varchar2, ParameterDirection.Input).Value = office.ContactPerson;
            cmd.Parameters.Add("password_in", OracleDbType.Varchar2, ParameterDirection.Input).Value = office.Password;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new Office
                {
                    OfficeNLK = Convert.ToInt32(dt.Rows[0]["OfficeNLK"].ToString())
                };
            }
            else
            {
                return new Office { };
            }
        }

        public IEnumerable<Office> GetUnverifiedOfficeList()
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETUNVERIFIEDOFFICESLIST";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new Office
                {
                    OfficeNLK = Convert.ToInt32(dr["OfficeNLK"].ToString()),
                    ApplicationLetterPath = dr["ApplicationLetterName"].ToString(),
                    ApplicationDate = dr["ApplicationDate"].ToString(),
                    Phone = dr["Phone"].ToString(),
                    ContactPerson = dr["ContactPerson"].ToString(),
                    Email = dr["Email"].ToString(),
                    Address = dr["Address"].ToString(),
                    OfficeName = dr["Name"].ToString(),
                    DistrictName = dr["DistrictName"].ToString(),
                    PradeshName = dr["PradeshName"].ToString(),
                    IsVerified = Convert.ToInt32(dr["IsVerified"].ToString())
                };
            }
        }

        public Office GetOfficeDetailsById(int officeId)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETOFFICEDETAILSBYID";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlk_id", OracleDbType.Int32, ParameterDirection.Input).Value = officeId;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new Office
                {
                    OfficeNLK = Convert.ToInt32(dt.Rows[0]["OfficeNLK"].ToString()),
                    ApplicationLetterPath = dt.Rows[0]["ApplicationLetterName"].ToString(),
                    ApplicationDate = dt.Rows[0]["ApplicationDate"].ToString(),
                    Phone = dt.Rows[0]["Phone"].ToString(),
                    ContactPerson = dt.Rows[0]["ContactPerson"].ToString(),
                    Email = dt.Rows[0]["Email"].ToString(),
                    Address = dt.Rows[0]["Address"].ToString(),
                    OfficeName = dt.Rows[0]["Name"].ToString(),
                    DistrictName = dt.Rows[0]["DistrictName"].ToString(),
                    PradeshName = dt.Rows[0]["PradeshName"].ToString(),
                    IsVerified = Convert.ToInt32(dt.Rows[0]["IsVerified"].ToString()),
                    VerifiedDate = dt.Rows[0]["VerifiedDate"].ToString()

                };
            }
            else
            {
                return new Office { };
            }
        }

        public void VerifyOffice(int officeId)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_VERIFYOFFICE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlk_id", OracleDbType.Int32, ParameterDirection.Input).Value = officeId;
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
        }
        public void RejectOffice(int officeId)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_REJECTOFFICE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlk_id", OracleDbType.Int32, ParameterDirection.Input).Value = officeId;
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
        }

        public IEnumerable<Office> GetVerifiedOfficeList()
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETVERIFIEDOFFICESLIST";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                yield return new Office
                {
                    OfficeNLK = Convert.ToInt32(dr["OfficeNLK"].ToString()),
                    ApplicationLetterPath = dr["ApplicationLetterName"].ToString(),
                    ApplicationDate = dr["ApplicationDate"].ToString(),
                    Phone = dr["Phone"].ToString(),
                    ContactPerson = dr["ContactPerson"].ToString(),
                    Email = dr["Email"].ToString(),
                    Address = dr["Address"].ToString(),
                    OfficeName = dr["Name"].ToString(),
                    DistrictName = dr["DistrictName"].ToString(),
                    PradeshName = dr["PradeshName"].ToString(),
                    IsVerified = Convert.ToInt32(dr["IsVerified"].ToString()),
                    VerifiedDate = dr["VerifiedDate"].ToString()
                };
            }
        }

        public string GetIdByNLK(string nlk)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETUSERIDBYNLK";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlk_id", OracleDbType.Int32, ParameterDirection.Input).Value = nlk;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0]["ID"].ToString();
        }
        public string GetOfficeNameByOfficeCode(string officeCode)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETOFFICENAMEBYOFFICECODE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("office_code", OracleDbType.Int32, ParameterDirection.Input).Value = officeCode;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return "कुनै अफिस भेटिएन.";
            }
            return dt.Rows[0]["Name"].ToString();
        }
        public DataTable GetYearId(int empID)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "select year_id from trans_d where emp_id = " + empID + " group by year_id order by year_id";
            cmd.CommandType = CommandType.Text;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public DataSet GetDetails(int empID, int yearId)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_GETDETAILS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("emp_idy", OracleDbType.Int32, ParameterDirection.Input).Value = empID;
            cmd.Parameters.Add("year_idy", OracleDbType.Int32, ParameterDirection.Input).Value = yearId;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("O_CURSOR1", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public bool IsUserRegistered(string userID)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_CHECKUSEREXISTS";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("idi", OracleDbType.NVarchar2, ParameterDirection.Input).Value = userID;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (Convert.ToInt32(dt.Rows[0]["USERCOUNT"]) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool IsUserOffice(string nlkid, string offId)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_VERIFYUSEROFFICE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlk_id", OracleDbType.NVarchar2, ParameterDirection.Input).Value = nlkid;
            cmd.Parameters.Add("off_id", OracleDbType.NVarchar2, ParameterDirection.Input).Value = offId;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (Convert.ToInt32(dt.Rows[0]["STATUS"]) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        //Functions for Password Change
        public bool VerifyUserForPasswordChange(string token, string nlkid)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_VERIFYUSERPASSWORDCHANGE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("nlkid", OracleDbType.NVarchar2, ParameterDirection.Input).Value = nlkid;
            cmd.Parameters.Add("token", OracleDbType.NVarchar2, ParameterDirection.Input).Value = token;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (Convert.ToInt32(dt.Rows[0]["STATUS"]) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool VerifyOfficeForPasswordChange(string token, string nlkid)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_VERIFYOFFICEPASSWORDCHANGE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("off_id", OracleDbType.NVarchar2, ParameterDirection.Input).Value = nlkid;
            cmd.Parameters.Add("token", OracleDbType.NVarchar2, ParameterDirection.Input).Value = token;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            string abc = dt.Rows[0]["STATUS"].ToString();
            if (Convert.ToInt32(dt.Rows[0]["STATUS"]) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Office OfficePasswordChange(string nlk, string password)
        {
            OracleDataAdapter da = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnx;
            cmd.InitialLONGFetchSize = 1000;
            cmd.CommandText = "USP_CHANGEPASSWORDREQUESTOFFC";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("off_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = nlk;
            cmd.Parameters.Add("newPassword", OracleDbType.Varchar2, ParameterDirection.Input).Value = password;
            cmd.Parameters.Add("O_CURSOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return new Office
                {
                    OfficeNLK = Convert.ToInt32(dt.Rows[0]["OfficeNLK"].ToString())
                };
            }
            else
            {
                return new Office { };
            }
        }
    }

}