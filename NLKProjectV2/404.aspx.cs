﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NLKProjectV2
{
    public partial class _404 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.IsInRole("Admin"))
            {
                link.HRef = "/admin";
            }
            else if (User.IsInRole("User"))
            {
                link.HRef = "/";
            }
            else if (User.IsInRole("corporate"))
            {
                link.HRef = "/office";
            }
            else
            {
                link.HRef = "/auth/login";
            }
        }
    }
}