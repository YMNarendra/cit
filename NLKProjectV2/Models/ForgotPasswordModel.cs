﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class ForgotPasswordModel
    {
        [Required(ErrorMessage="NLK Number is required.")]
        public string NLKNo { get; set; }
        [EmailAddress(ErrorMessage="Invalid e-mail address"),  Required(ErrorMessage="Email is required.")]
        public string Email { get; set; }

    }
}