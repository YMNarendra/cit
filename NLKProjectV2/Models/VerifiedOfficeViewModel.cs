﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class VerifiedOfficeViewModel
    {
        public Office Office { get; set; }
        public IEnumerable<User> Staffs { get; set; }
    }
}