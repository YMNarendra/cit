﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class AccountModel
    {
        public string Id { get; set; }
        public string OpeningBalance { get; set; }
        public string YearID { get; set; }
        public decimal? Deposit { get; set; }
        public string Interest { get; set; }
        public string OpeningInterest { get; set; }
        public string InterestRate { get; set; }
        public string YearDays { get; set; }
        public string YearName { get; set; }
    }
}