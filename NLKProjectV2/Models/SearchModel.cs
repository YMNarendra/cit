﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class SearchModel
    {
        public Nullable<int> SearchID { get; set; }
        public string SearchEmail { get; set; }
        public string SearchMobile { get; set; }
    }
}