﻿using NLKProjectV2.DbContexts;
using NLKProjectV2.Models;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Utilities;

namespace NLKProjectV2.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private AuthDbContext dbContext;
        private DateConverter dc;
        public AuthController()
        {
            dbContext = new AuthDbContext();
            dc = new DateConverter();
        }
        //
        // GET: /Auth/
        [HttpGet]
        public ActionResult LogIn(string returnUrl)
        {

            //if (returnUrl.ToLower() == "/office/getofficename/1")
            //{
            //    GetOfficeName(1);
            //    return RedirectToAction("corporate/signup", "auth");
            //}
            var model = new LogInModel
            {
                ReturnUrl = returnUrl
            };



            return View(model);
        }

        [HttpPost]
        public ActionResult LogIn(LogInModel model)
        {
            string role = "";
            if (!ModelState.IsValid)
            {
                return View();
            }

            try
            {
                string password = CreateMD5(model.Password);
                User user = dbContext.ValidateUser(Convert.ToInt32(model.UserNo), password);

                if (user.IsActive == 0)
                {
                    ModelState.AddModelError("AuthFailed", "Your account is inactive !");
                    return View();
                }

                if (user.NLKNo == model.UserNo)
                {
                    dbContext.InsertLogDetails(user.NLKNo);
                    role = "user";
                    var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Role, "User"),
                            new Claim(ClaimTypes.Name, user.Name),
                            new Claim("NLK_No", user.NLKNo),
                            new Claim("UserID", user.UserId.ToString())
                    },
                        "ApplicationCookie");

                    var ctx = Request.GetOwinContext();
                    var authManager = ctx.Authentication;

                    authManager.SignIn(identity);

                    return Redirect(GetRedirectUrl(model.ReturnUrl, role));
                }
                // user authN failed
                ModelState.AddModelError("AuthFailed", "The NLK Number or Password you entered is incorrect!");
                return View();
            }
            catch (Exception Ex)
            {
                ModelState.AddModelError("AuthFailed", "The NLK Number or Password you entered is incorrect!" + Ex.Message);
                return View();
            }

        }

        private static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }

        private string GetRedirectUrl(string returnUrl, string role)
        {
            if (role == "user")
            {
                if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                {
                    return Url.Action("index", "home", new { yearid = 0 });
                }
            }

            if (role == "admin")
            {
                if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                {
                    return Url.Action("index", "admin");
                }
            }

            if (role == "corporate")
            {
                if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                {
                    return Url.Action("index", "office");
                }
            }

            return returnUrl;
        }

        public ActionResult LogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("index", "home");
        }
        public ActionResult AdminLogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("admin", "auth");
        }

        //USER SIGNUP
        public ActionResult SignUp()
        {
            ViewBag.Districts = dbContext.GetDistrictList();
            ViewBag.Pradeshs = dbContext.GetPradeshList();
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(RegistrationModel model)
        {
            try
            {
                ViewBag.Districts = dbContext.GetDistrictList();
                ViewBag.Pradeshs = dbContext.GetPradeshList();
                string filename1 = "", filename2 = "";
                if (!ModelState.IsValid)
                {
                    return View();
                }
                int flag = 0;
                if (model.NlkIdImageBack != null)
                {
                    int file = model.NlkIdImageBack.ContentLength;

                    if (file < 512000)
                    {
                        string ext2 = Path.GetExtension(model.NlkIdImageBack.FileName).ToLower();
                        if (ext2 == ".png" || ext2 == ".jpg" || ext2 == ".jpeg" || ext2 == ".gif" || ext2 == ".bmp")
                        {
                            filename2 = model.NLKNumber + "_back" + ext2;
                            string path2 = System.IO.Path.Combine(Server.MapPath("~/uploads/"), filename2);
                            model.NlkIdImageBack.SaveAs(path2);
                        }
                        else
                        {
                            ModelState.AddModelError("", "The NLK ID back image was not in correct format.[FORMATS: jpg,png,gif,bmp]");
                            return View();
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "The NLK ID back image size was more than 500KB");
                        return View();
                    }

                }
                if (model.NlkIdImageFront != null)
                {
                    int file = model.NlkIdImageBack.ContentLength;

                    if (file < 512000)
                    {
                        string ext1 = Path.GetExtension(model.NlkIdImageFront.FileName).ToLower();
                        if (ext1 == ".png" || ext1 == ".jpg" || ext1 == ".jpeg" || ext1 == ".gif" || ext1 == ".bmp")
                        {
                            filename1 = model.NLKNumber + "_front" + ext1;
                            string path1 = System.IO.Path.Combine(Server.MapPath("~/uploads/"), filename1);
                            model.NlkIdImageFront.SaveAs(path1);
                            flag = 1;
                        }
                        else
                        {
                            ModelState.AddModelError("", "The NLK ID front image was not in correct format.[FORMATS: jpg,png,gif,bmp]");
                            return View();
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "The NLK ID back image size was more than 500KB");
                        return View();
                    }


                }
                if (flag == 1)
                {
                    model.Password = CreateMD5(model.Password);
                    model.PassDate = dc.ADtoBS(DateTime.Now);
                    model.LoginFail = 0;
                    model.Time = ConvertToTimestamp(DateTime.Now);
                    model.DefaultPass = "Yes";
                    model.State = "Block";
                    model.Validate = "1";
                    model.imFrontName = filename1;
                    model.imBackName = filename2;
                    bool check = dbContext.IsUserRegistered(model.NLKNumber);
                    if (!check)
                    {
                        if (model.DateOfBirth == null)
                        {
                            model.DateOfBirth = Convert.ToDateTime("01/01/1901");
                        }
                        RegistrationModel backData = dbContext.InsertUser(model);
                        //SENDING MAIL

                        StringBuilder builder = new StringBuilder();
                        builder.Append("The Registration for the user with ID " + model.NLKNumber + " has been received.");
                        builder.AppendLine();
                        builder.Append("The information provided will be reviewed by the administrator and will notify you!");
                        builder.AppendLine();
                        builder.Append("NAGARIK LAGANI KOSH");
                        builder.AppendLine();
                        builder.Append("Baneshwor, Kathmandu");


                        var client = new SmtpClient("smtp.gmail.com", 587)
                        {
                            Credentials = new NetworkCredential("eservices.nlk@gmail.com", "eservice@cit1"),
                            EnableSsl = true
                        };
                        client.Send("eservices.nlk@gmail.com", model.Email,
                            "Registration done at Nagarik Lagani Kosh", builder.ToString());

                        return RedirectToAction("Thankyou", "Auth", new { msg = backData.NLKNumber });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The user with NLK ID :" + model.NLKNumber + " is already registered!");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("ImageProblem", "There is some problem with your images.");
                    return View();
                }
            }
            catch (Exception Ex)
            {
                ModelState.AddModelError("AuthProblem", Ex.Message);
                return View();
            }

        }

        private double ConvertToTimestamp(DateTime value)
        {
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            return (double)span.TotalSeconds;
        }

        public ActionResult Thankyou(string msg)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            ViewBag.Message = msg;
            return View();
        }

        //FUNCTIONS FOR ADMIN LOGIN
        [Route("eservicemanager")]
        [HttpGet]
        public ActionResult Admin(string returnUrl)
        {
            var model = new LogInModel
            {
                ReturnUrl = returnUrl
            };

            return View(model);
        }
        [Route("eservicemanager")]
        [HttpPost]
        public ActionResult Admin(LogInModel model)
        {
            string role = "";
            if (!ModelState.IsValid)
            {
                return View();
            }
            //model.Password = "518600f90d8bc0a293beac816834c884";

            model.Password = CreateMD5(model.Password);
            User adminUser = dbContext.ValidateAdmin(model.UserNo, model.Password.ToLower());
            if (adminUser.Name == model.UserNo)
            {
                role = "admin";
                var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Role, "Admin"),
                     new Claim(ClaimTypes.Name, adminUser.Name)
                    },
                    "ApplicationCookie");
                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
                return Redirect(GetRedirectUrl(model.ReturnUrl, role));
            }
            ModelState.AddModelError("AuthFailed", "The Username or Password you entered is incorrect!");
            return View();
        }

        [HttpGet]
        public ActionResult ForgotPAssword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgotPAssword(ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                byte[] key = Guid.NewGuid().ToByteArray();
                string token = Convert.ToBase64String(time.Concat(key).ToArray());
                User user = dbContext.VerifyEmailAddress(model.NLKNo, model.Email);
                string Url = Request.Url.OriginalString;
                if (user.Email == model.Email)
                {
                    dbContext.InsertPasswordInfo(model.NLKNo, token, 0);
                    //var request = HttpContext.Current.Request;
                    StringBuilder builder = new StringBuilder();
                    builder.Append("There has been a password change request from your account in Nagarik Lagani Kosh.");
                    builder.AppendLine();
                    builder.Append("Click the link below to reset your password.");
                    builder.AppendLine();
                    builder.Append("http://eservice.nlk.org.np/auth/passwordchange?token=" + token + "&nlkid=" + model.NLKNo);
                    builder.AppendLine();
                    builder.Append("NAGARIK LAGANI KOSH");
                    builder.AppendLine();
                    builder.Append("Baneshwor, Kathmandu");
                    var client = new SmtpClient("smtp.gmail.com", 587)
                    {
                        Credentials = new NetworkCredential("eservices.nlk@gmail.com", "eservice@cit1"),
                        EnableSsl = true
                    };
                    client.Send("eservices.nlk@gmail.com", model.Email,
                        "Forgot Password Link Activation", builder.ToString());

                    return RedirectToAction("PasswordRequestSent", "Auth");
                }
                else
                {
                    ModelState.AddModelError("", "The NLK number and e-mail you provided do not match!");
                    return View();
                }
            }
            catch (Exception Ex)
            {
                ModelState.AddModelError("", Ex.Message);
                return View();
            }
        }

        public ActionResult PasswordChange(string token, string nlkid)
        {
            token = token.Replace(" ", "+");
            ViewBag.IsGenuineUser = dbContext.VerifyUserForPasswordChange(token, nlkid);
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult PasswordChange(ForgotPasswordViewModel model)
        {
            model.Token = model.Token.Replace(" ", "+");
            ViewBag.IsGenuineUser = dbContext.VerifyUserForPasswordChange(model.Token, model.NLKId);
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (model.NewPassword == model.NewPasswordConfirm)
            {
                var user = dbContext.UserPasswordChange(model.NLKId, CreateMD5(model.NewPassword).ToLower());
                ViewBag.Id = user.Name;
                ViewBag.Success = "The Password has been changed";
                return RedirectToAction("PasswordChangedFromRequest", "Auth");
            }
            else
            {
                ModelState.AddModelError("", "The passwords do not match");
                return View();
            }

        }

        public ActionResult PasswordChangedFromRequest()
        {
            return View();
        }

        public ActionResult PasswordRequestSent()
        {
            return View();
        }



        //CORPORATE AUTHENTICATION FUNCTIONS
        [Route("auth/corporatelogin")]
        public ActionResult OfficeLogin(string returnUrl)

        {
            var model = new LogInModel
            {
                ReturnUrl = returnUrl
            };
            return View();
        }
        [HttpPost]
        [Route("auth/corporatelogin")]
        public ActionResult OfficeLogin(OfficeLogInViewModel model)
        {
            string role = "";
            if (!ModelState.IsValid)
            {
                return View();
            }
            model.Password = CreateMD5(model.Password);
            Office corporate = dbContext.ValidateOffice(model.OfficeId, model.Password.ToLower());
            if (corporate.OfficeNLK.ToString() == model.OfficeId)
            {
                role = "corporate";
                var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Role, "corporate"),
                    new Claim(ClaimTypes.Name, corporate.OfficeName),
                    new Claim("OfficeID", corporate.OfficeNLK.ToString())
                },
                    "ApplicationCookie");
                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);
                return Redirect(GetRedirectUrl(model.ReturnUrl, role));
            }
            ModelState.AddModelError("AuthFailed", "The Username or Password you entered is incorrect!");
            return View();
        }
        [Route("auth/corporate/signup")]
        public ActionResult OfficeSignUp()
        {
            ViewBag.Districts = dbContext.GetDistrictList();
            ViewBag.Pradeshs = dbContext.GetPradeshList();
            return View();
        }
        [Route("auth/corporate/signup")]
        [HttpPost]
        public ActionResult OfficeSignUp(Office model)
        {
            ViewBag.Districts = dbContext.GetDistrictList();
            ViewBag.Pradeshs = dbContext.GetPradeshList();
            try
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }
                int flag = 0; string filename1 = "";
                if (model.ApplicationLetter != null)
                {
                    int file = model.ApplicationLetter.ContentLength;

                    if (file < 512000)
                    {
                        string ext1 = Path.GetExtension(model.ApplicationLetter.FileName).ToLower();
                        if (ext1 == ".png" || ext1 == ".jpg" || ext1 == ".jpeg" || ext1 == ".gif" || ext1 == ".bmp")
                        {
                            filename1 = model.OfficeNLK + "_application" + ext1;
                            string path1 = System.IO.Path.Combine(Server.MapPath("~/uploads/officeapplications"), filename1);
                            model.ApplicationLetter.SaveAs(path1);
                            flag = 1;
                        }
                        else
                        {
                            ModelState.AddModelError("ImageProblem", "The file you are trying to upload is not an image.");
                            return View();
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "The NLK ID back image size was more than 500KB");
                        return View();
                    }


                }
                if (flag == 1)
                {
                    model.Password = CreateMD5(model.Password);
                    model.ApplicationLetterPath = filename1;
                    Office backData = dbContext.InsertOffice(model);

                    StringBuilder builder = new StringBuilder();
                    builder.Append("The Registration for the user with ID " + model.OfficeNLK + " has been received.");
                    builder.AppendLine();
                    builder.Append("The information provided will be reviewed by the administrator and you will be notified!");
                    builder.AppendLine();
                    builder.Append("NAGARIK LAGANI KOSH");
                    builder.AppendLine();
                    builder.Append("Baneshwor, Kathmandu");


                    var client = new SmtpClient("smtp.gmail.com", 587)
                    {
                        Credentials = new NetworkCredential("eservices.nlk@gmail.com", "eservice@cit1"),
                        EnableSsl = true
                    };
                    client.Send("eservices.nlk@gmail.com", model.Email,
                        "Registration done at Nagarik Lagani Kosh", builder.ToString());

                    return RedirectToAction("ThankyouOffice", "Auth", new { msg = backData.OfficeNLK });
                }
                else
                {
                    ModelState.AddModelError("ImageProblem", "There is some problem with your images.");
                    return View();
                }
            }
            catch (Exception Ex)
            {
                ModelState.AddModelError("AuthProblem", "Cannot register this office. There is some problem!");
                return View();
            }
            // return View();
        }
        [Route("auth/corporate/thankyou")]
        public ActionResult ThankyouOffice(string msg)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            ViewBag.Message = msg;
            return View();
        }
        public ActionResult OfficeLogout()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;
            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("OfficeLogin", "auth");
        }

        [Route("auth/corporate/forgotpassword")]
        public ActionResult OfficeForgotPassword()
        {
            return View();
        }
        [HttpPost]
        [Route("auth/corporate/forgotpassword")]
        public ActionResult OfficeForgotPassword(OfficeForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                byte[] key = Guid.NewGuid().ToByteArray();
                string token = Convert.ToBase64String(time.Concat(key).ToArray());
                Office office = dbContext.VerifyEmailAddressOffice(model.OfficeID, model.Email);
                if (office.Email == model.Email && office.IsVerified == 1)
                {
                    dbContext.InsertPasswordInfoOffice(model.OfficeID, token, 0);

                    StringBuilder builder = new StringBuilder();
                    builder.Append("There has been a password change request from your account in Nagarik Lagani Kosh.");
                    builder.AppendLine();
                    builder.Append("Click the link below to reset your password.");
                    builder.AppendLine();
                    builder.Append("http://202.70.86.25/auth/corporate/passwordchange?token=" + token + "&nlkid=" + model.OfficeID);
                    builder.AppendLine();
                    builder.Append("NAGARIK LAGANI KOSH");
                    builder.AppendLine();
                    builder.Append("Baneshwor, Kathmandu");
                    var client = new SmtpClient("smtp.gmail.com", 587)
                    {
                        Credentials = new NetworkCredential("eservices.nlk@gmail.com", "eservice@cit1"),
                        EnableSsl = true
                    };
                    client.Send("eservices.nlk@gmail.com", model.Email,
                        "Forgot Password Link Activation", builder.ToString());

                    return RedirectToAction("PasswordRequestSentOffice", "Auth");
                }
                else
                {
                    ModelState.AddModelError("", "The NLK number and e-mail you provided do not match!\n The ID may not yet be registered!");
                    return View();
                }
            }
            catch (Exception Ex)
            {
                ModelState.AddModelError("", Ex.Message);
                return View();
            }
        }
        [Route("auth/corporate/passwordrequestsent")]
        public ActionResult PasswordRequestSentOffice()
        {
            return View();
        }
        [Route("auth/corporate/passwordchange")]
        public ActionResult PasswordChangeOffice(string token, string nlkid)
        {
            ViewBag.IsGenuineOffice = dbContext.VerifyOfficeForPasswordChange(token, nlkid);
            return View();
        }
        [Route("auth/corporate/passwordchange")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordChangeOffice(ForgotPasswordViewModel model)
        {
            ViewBag.IsGenuineOffice = dbContext.VerifyOfficeForPasswordChange(model.Token, model.NLKId);
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (model.NewPassword == model.NewPasswordConfirm)
            {
                var user = dbContext.OfficePasswordChange(model.NLKId, CreateMD5(model.NewPassword).ToLower());
                ViewBag.Id = user.OfficeNLK;
                ViewBag.Success = "The Password has been changed";
                return RedirectToAction("PasswordChangedFromRequestOffice", "Auth");
            }
            else
            {
                ModelState.AddModelError("", "The passwords do not match");
                return View();
            }

        }
        public ActionResult PasswordChangedFromRequestOffice()
        {
            return View();
        }
        public JsonResult GetOfficeName(string id)
        {

            int result;
            if (int.TryParse(id, out result))
            {
                id = result.ToString();
            }
            else
            {
                id = "0";
            }
            string lst = dbContext.GetOfficeNameByOfficeCode(id);
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}