﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NLKProjectV2.Models
{
    public class OfficeLogInViewModel
    {
        [Required(ErrorMessage = "The Password is required !")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [HiddenInput]
        public string ReturnUrl { get; set; }
        [Required(ErrorMessage = "Cit ID is required")]
        public string OfficeId { get; set; }
    }
}