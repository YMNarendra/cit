﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class ChangePasswordModel
    {
        [Required(ErrorMessage="The NLK NUmber is required.")]
        public string NLKNo { get; set; }
        [Required(ErrorMessage="The Password is required.")]
        public string Password { get; set; }
        [Required(ErrorMessage="The New Password field is required.")]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string NewPasswordConfirm { get; set; }
    }
}