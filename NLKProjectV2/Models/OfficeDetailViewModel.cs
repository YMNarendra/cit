﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class OfficeDetailViewModel
    {
        public string OfficeID { get; set; }
        public string OfficeName { get; set; }
        public string OfficeAddress { get; set; }
        public IEnumerable<User> Employees { get; set; }
    }
}