﻿using System;
using System.Web.Mvc;

namespace NLKProjectV2.App_Start
{
    public class RequreSecureConnectionFilter : RequireHttpsAttribute
    {
        //public RequreSecureConnectionFilter()
        //{
        //}
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (filterContext.HttpContext.Request.IsLocal)
            {
                // when connection to the application is local, don't do any HTTPS stuff
                return;
            }

            base.OnAuthorization(filterContext);
        }
        //public bool IsLocal
        //{
        //    get
        //    {
        //        String remoteAddress = UserHostAddress;

        //        // if unknown, assume not local
        //        if (String.IsNullOrEmpty(remoteAddress))
        //            return false;

        //        // check if localhost
        //        if (remoteAddress == "127.0.0.1" || remoteAddress == "::1")
        //            return true;

        //        // compare with local address
        //        if (remoteAddress == LocalAddress)
        //            return true;

        //        return false;
        //    }
        //}
    }
}