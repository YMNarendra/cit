﻿using NLKProjectV2.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLKProjectV2.Models;
using System.Text;
using System.Data;
using System.IO;

namespace NLKProjectV2.Controllers
{
    [Authorize(Roles="Admin,User")]
    public class HomeController : BaseController
    {
        AuthDbContext ctx = new AuthDbContext();
        //
        // GET: /Home/
        public ActionResult Index()

        {
            ViewBag.Title = "Home" + " | " + "Transaction";
            var identity = (System.Security.Claims.ClaimsIdentity)User.Identity;
            ViewBag.LoginUserId = identity.FindFirst("UserID").Value;
            if (User.IsInRole("User"))
            {
                ViewBag.data = ctx.GetTransactionDetails1(Convert.ToInt32(identity.FindFirst("UserID").Value), 0);

                var transDetail = ctx.GetTransactionDetails(Convert.ToInt32(identity.FindFirst("UserID").Value), 0);
                ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
                ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
                //ViewBag.IntrestSum = transDetail.Transactions.Sum(x => Convert.ToDecimal( x.Interest != ""? x.Interest:"0"));
                return View(transDetail);
            }
            else
            {
                return RedirectToAction("Login","Auth");
            }
        }

        [HttpPost]
        public ActionResult Index(int YearValue)
        {
            ViewBag.Title = "Home" + " | " + "Transaction";
            ViewBag.YearValue = YearValue;
            var identity = (System.Security.Claims.ClaimsIdentity)User.Identity;

            //if (YearValue == 0)
            //{
               
                   ViewBag.data = ctx.GetTransactionDetails1(Convert.ToInt32(identity.FindFirst("UserID").Value),YearValue);
                
                var transDetail = ctx.GetTransactionDetails(Convert.ToInt32(identity.FindFirst("UserID").Value), YearValue);
                ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
            ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
            //ViewBag.IntrestSum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Interest != "" ? x.Interest : "0"));

            return View(transDetail);
            //}
            //else
            //{
            //    ViewBag.data = ctx.GetTransactionDetails1(Convert.ToInt32(identity.FindFirst("UserID").Value), YearValue);

            //    var transDetail = ctx.GetTransactionDetails(Convert.ToInt32(identity.FindFirst("UserID").Value), YearValue);
            //    ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
            //    //ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
            //    //ViewBag.IntrestSum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Interest != "" ? x.Interest : "0"));

            //    return View(transDetail);
            //}
           
        }

        public ActionResult ViewProfile(int? id)
        {
            ViewBag.Title = "Home" + " | " + "Profile";
            ViewBag.Districts = ctx.GetDistrictList();
            ViewBag.Pradeshs = ctx.GetPradeshList();
            User user = ctx.GetUserById(id.GetValueOrDefault());
            return View(user);
        }
        [HttpGet]
        public ActionResult ChangePassword()
        {
            ViewBag.Title = "Home" + " | " + "Change Password";
            var identity = (System.Security.Claims.ClaimsIdentity)User.Identity;
            ViewBag.NLKNo = identity.FindFirst("NLK_No").Value;
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel user)
        {
            var identity = (System.Security.Claims.ClaimsIdentity)User.Identity;
            ViewBag.NLKNo = identity.FindFirst("NLK_No").Value;
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (user.NewPassword == user.NewPasswordConfirm)
            {
                string password = CreateMD5(user.Password);
                string newPassword = CreateMD5(user.NewPassword);
                User reuser = ctx.ChangePassword(user.NLKNo, password, newPassword);
                return RedirectToAction("passwordChanged", "home", new { msg = reuser.Name });
            }
            else
            {
                ModelState.AddModelError("AuthFailed", "Passwords do not match.");
                return View();
            }
        }

        public ActionResult PasswordChanged(string msg)
        {
            ViewBag.NLKNo = msg;
            return View();
        }

        private static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }

        [HttpPost]
        public RedirectResult UpdateuserInfo(User user)
        {
           
            if (user.NlkIdImageBack != null)
            {
                int file = user.NlkIdImageBack.ContentLength;

                if (file < 512000)
                {
                    string ext2 = Path.GetExtension(user.NlkIdImageBack.FileName).ToLower();
                    if (ext2 == ".png" || ext2 == ".jpg" || ext2 == ".jpeg" || ext2 == ".gif" || ext2 == ".bmp")
                    {
                        user.imFrontName = user.NLKNo + "_back" + ext2;
                        string path2 = System.IO.Path.Combine(Server.MapPath("~/uploads/"), user.imFrontName);
                        user.NlkIdImageBack.SaveAs(path2);
                    }
                    else
                    {
                        ModelState.AddModelError("", "The NLK ID back image was not in correct format.[FORMATS: jpg,png,gif,bmp]");
                        Redirect("/home/viewprofile/" + user.NLKNo);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The NLK ID back image size was more than 500KB");
                    Redirect("/home/viewprofile/" + user.NLKNo);
                }

            }
            if (user.NlkIdImageFront != null)
            {
                int file = user.NlkIdImageFront.ContentLength;

                if (file < 512000)
                {
                    string ext1 = Path.GetExtension(user.NlkIdImageFront.FileName).ToLower();
                    if (ext1 == ".png" || ext1 == ".jpg" || ext1 == ".jpeg" || ext1 == ".gif" || ext1 == ".bmp")
                    {
                        user.imBackName = user.NLKNo + "_front" + ext1;
                        string path1 = System.IO.Path.Combine(Server.MapPath("~/uploads/"), user.imBackName);
                        user.NlkIdImageFront.SaveAs(path1);
                    }
                    else
                    {
                        ModelState.AddModelError("", "The NLK ID front image was not in correct format.[FORMATS: jpg,png,gif,bmp]");
                        Redirect("/home/viewprofile/" + user.NLKNo);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The NLK ID back image size was more than 500KB");
                    Redirect("/home/viewprofile/" + user.NLKNo);
                }
            }
            ctx.UpdateUserInfoData(user.NLKNo, user.Email, user.MobileNo, user.FatherName, user.MotherName, user.GrandfatherName, 
                user.GrandMotherName, user.Gender, user.CitizenshipIssueDistrictId, user.CitzDate, user.PradeshId,
                user.SpouseName, user.SpouseCitzNo, user.SpouseCitzDate, user.PAddress, user.TAddress, user.imFrontName, user.imBackName);
            return Redirect("/home/viewprofile/"+user.NLKNo);
        }

        public ActionResult Test()
        {
            var identity = (System.Security.Claims.ClaimsIdentity)User.Identity;
            //var transDetail = ctx.GetTransactionDetails(Convert.ToInt32(identity.FindFirst("UserID").Value), yearid);
            DataTable dt = ctx.GetYearId(Convert.ToInt32(identity.FindFirst("UserID").Value));
            ViewBag.Years = dt;
            foreach (DataRow dr in dt.Rows)
            {
                int id = Convert.ToInt32(identity.FindFirst("UserID").Value);
                int yearID = Convert.ToInt32(dr["year_id"].ToString());
                ViewBag.Datab  = ctx.GetDetails(id,yearID);
            }
            return View();
        }
        
    }
}