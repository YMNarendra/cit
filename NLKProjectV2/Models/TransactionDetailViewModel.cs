﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public class TransactionDetailViewModel
    {
        public int EmployeeId { get; set; }
        public User EmployeeDetail { get; set; }
        public IEnumerable<Year> YearList { get; set; }
        public Office OfficeDetail { get; set; }
        public Office ImmediateOffice { get; set; }
        public IEnumerable<Transaction> Transactions { get; set; }
        public IEnumerable<AccountModel> Accounts { get; set; }
        public double Sum { get; set; }

    }
}