﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLKProjectV2.Models
{
    public sealed class District
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NepaliName { get; set; }
    }
}