﻿using NLKProjectV2.DbContexts;
using NLKProjectV2.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;

namespace NLKProjectV2.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private AuthDbContext _context;
        public AdminController()
        {
            _context = new AuthDbContext();
        }
        //
        // GET: /Admin/
        public ActionResult Index(int? page)
        {
            ViewBag.Title = "Home | Unverified User";
            IEnumerable<User> userList = _context.GetUserList().OrderBy(x => x.CreatedDate);
            return View(userList.ToList().ToPagedList(page ?? 1, 30));
        }
        [HttpPost]
        public ActionResult Index(SearchModel searchModel, int? page)
        {
            ViewBag.Title = "Home";
            ViewBag.Mobile = searchModel.SearchMobile;
            if (searchModel.SearchID == null)
            {
                searchModel.SearchID = 0;
            }
            ViewBag.SearchParameters = searchModel;
            IEnumerable<User> userList =
                _context.SearchUnverifiedUsers(searchModel.SearchID, searchModel.SearchEmail, searchModel.SearchMobile);
            return View(userList.ToList().ToPagedList(page ?? 1, 10));
        }
        public ActionResult ViewDetails(int id)
        {
            ViewBag.Title = "Home" + " | " + "Details";
            User user = _context.GetUserById(id);
            return View(user);
        }
        public ActionResult VerifiedUserDetails(int id)
        {
            ViewBag.Title = "Home" + " | " + "Verified User Detail";
            User user = _context.GetUserById(id);
            return View(user);
        }



        public ActionResult ChangeUserPassword(string id)
        {
            ViewBag.Title = "Home" + " | " + "Password Change For User";
            var model = new ForgotPasswordViewModel();
            model.NLKId = id;
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeUserPassword(ForgotPasswordViewModel model)
        {
            try
            {
                ViewBag.Title = "Home" + " | " + "Password Change For User";
                var user = _context.UserPasswordChange(model.NLKId, CreateMD5(model.NewPassword).ToLower());
                ViewBag.Id = user.Name;
                ViewBag.Success = "The Password has been changed";
                return RedirectToAction("VerifiedUserDetails", new { id = Convert.ToInt32(ViewBag.Id) });
            }
            catch (Exception e)
            {

                TempData["Message"] = $"Something went wrong: {e.Message}";
                return View(model);
            }
        }


        public ActionResult ChangeOfficePassword(string id)
        {
            ViewBag.Title = "Home" + " | " + "Password Change For Office";
            var model = new ForgotPasswordViewModel();
            model.NLKId = id;
            return View(model);

        }

        [HttpPost]
        public ActionResult ChangeOfficePassword(ForgotPasswordViewModel model)
        {
            try
            {
                ViewBag.Title = "Home" + " | " + "Password Change For Office";
                var user = _context.OfficePasswordChange(model.NLKId, CreateMD5(model.NewPassword).ToLower());
                ViewBag.Id = user.OfficeNLK;
                ViewBag.Success = "The Password has been changed";
                return RedirectToAction("VerifiedOfficeDetail", new { id = Convert.ToInt32(ViewBag.Id) });

            }
            catch (Exception e)
            {

                TempData["Message"] = $"Something went wrong: {e.Message}";
                return View(model);
            }
        }



        public ActionResult VerifyUser(int id)
        {
            ViewBag.Title = "Home" + " | " + "Verify User";
            User user = _context.VerifyUser(id);
            User userInfo = _context.GetUserById(id);
            StringBuilder builder = new StringBuilder();
            builder.Append("Your account at Nagarik Lagani Kosh with ID " + id + " has been Verified!");
            builder.AppendLine();
            builder.Append("Now You can login with the NLK ID and Password you provided at the time of Registration.");
            builder.AppendLine();
            builder.Append("In case you forgot your password, you can always reset your password at");
            builder.AppendLine();
            builder.Append("http://202.70.86.25/auth/forgotpassword");
            builder.AppendLine();
            builder.Append("NAGARIK LAGANI KOSH");
            builder.AppendLine();
            builder.Append("Baneshwor, Kathmandu");

            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("eservices.nlk@gmail.com", "eservice@cit1"),
                EnableSsl = true
            };
            client.Send("eservices.nlk@gmail.com", userInfo.Email,
                "Account activated at Nagarik Lagani Kosh", builder.ToString());
            return Redirect("/admin");
        }
        public ActionResult RejectUser(int id, string message)
        {
            ViewBag.Title = "Home" + " | " + "Reject User";
            User userInfo = _context.GetUserById(id);
            StringBuilder builder = new StringBuilder();
            builder.Append("Your application to register for e-services has been rejected.");
            builder.AppendLine();
            builder.Append(message);
            builder.AppendLine();
            builder.Append("Contact IT Department at Nagarik Lagani Kosh for further details.");
            builder.AppendLine();
            builder.Append("Phone: 4781673 / 4785319 / 4785325 ");
            builder.AppendLine();
            builder.AppendLine();
            builder.Append("NAGARIK LAGANI KOSH");
            builder.AppendLine();
            builder.Append("Baneshwor, Kathmandu");

            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("eservices.nlk@gmail.com", "eservice@cit1"),
                EnableSsl = true
            };
            client.Send("eservices.nlk@gmail.com", userInfo.Email,
                "Application for Registration Rejected", builder.ToString());

            User user = _context.RejectUser(id);
            string path = Server.MapPath("~\\Uploads\\");
            if (System.IO.File.Exists(path + user.Ext))
            {
                System.IO.File.Delete(path + user.Ext);
            }
            if (System.IO.File.Exists(path + user.Ext2))
            {
                System.IO.File.Delete(path + user.Ext2);
            }
            return RedirectToAction("Index", "Admin");
        }
        public ActionResult VerifiedUsers(int? page)
        {
            ViewBag.Title = "Home" + " | " + "Verified Users";
            IEnumerable<User> verifiedUserList = _context.GetVerifiedUsersList();
            return View(verifiedUserList.ToList().ToPagedList(page ?? 1, 30));
        }
        [HttpPost]
        public ActionResult VerifiedUsers(SearchModel searchModel, int? page)
        {
            ViewBag.Title = "Home" + " | " + "Verified Users";
            ViewBag.Mobile = searchModel.SearchMobile;
            if (searchModel.SearchID == null)
            {
                searchModel.SearchID = 0;
            }
            ViewBag.SearchParameters = searchModel;
            IEnumerable<User> userList = _context.SearchVerifiedUsers(searchModel.SearchID, searchModel.SearchEmail, searchModel.SearchMobile);
            return View(userList.ToList().ToPagedList(page ?? 1, 10));
        }

        public ActionResult DeleteVerifiedUser(int id)
        {
            _context.DeleteVerifiedUser(id.ToString());
            return RedirectToAction("VerifiedUsers", "Admin");
        }
        public ActionResult DeleteVerifiedOffice(int id)
        {
            _context.DeleteVerifiedOfice(id.ToString());
            return RedirectToAction("verifiedoffices", "Admin");
        }
        public ActionResult OfficeDetail()
        {
            ViewBag.Title = "Home" + " | " + "Office Detail";
            return View();
        }
        [HttpPost]
        public ActionResult OfficeDetail(string officeID)
        {
            ViewBag.Title = "Home" + " | " + "Office Detail";
            try
            {
                var userList = _context.GetUsersByOfficeID(officeID).ToList();
                if (userList.Count == 0)
                {
                    TempData["ErrorMessage"] = $"Couldnt find the data for the office";
                    return View();

                }
                ViewBag.ID = officeID;
                return View(userList);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = $"Something went wrong: {ex.Message}";
                return View();
            }
        }

        public ActionResult ClientDetail()
        {
            ViewBag.Title = "Home" + " | " + "Client Detail";
            return View();
        }

        public ActionResult UserAccount(string id)
        {

            ViewBag.Title = "Home" + " | " + "User Detail";
            int idint;
            if (int.TryParse(id, out idint))
            {
                try
                {
                    int userId = Convert.ToInt32(_context.GetIdByNLK(id));
                    ViewBag.data = _context.GetTransactionDetails1(Convert.ToInt32(userId), 0);

                    var transDetail = _context.GetTransactionDetails(Convert.ToInt32(userId), 0);
                    ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
                    ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
                    return View(transDetail);
                }
                catch (Exception Ex)
                {
                    ModelState.AddModelError("", Ex.Message);
                    return View();
                }
            }
            else
            {
                ModelState.AddModelError("", "The NLK ID for this user is not valid!");
                return View();
            }
        }

        [HttpPost]
        public ActionResult UserAccount(string id, string YearValue)
        {

            try
            {
                if (YearValue == null)
                {
                    YearValue = "0";
                }
                ViewBag.Title = "Home" + " | " + "User Detail";

                int yearvalue = Convert.ToInt32(YearValue);
                ViewBag.YearValue = yearvalue;
                ViewBag.YearValue = yearvalue;
                var identity = (System.Security.Claims.ClaimsIdentity)User.Identity;
                int userId = Convert.ToInt32(_context.GetIdByNLK(id));
                if (yearvalue == 0)
                {

                    ViewBag.data = _context.GetTransactionDetails1(Convert.ToInt32(userId), yearvalue);

                    var transDetail = _context.GetTransactionDetails(Convert.ToInt32(userId), yearvalue);
                    ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
                    ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
                    //ViewBag.IntrestSum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Interest != "" ? x.Interest : "0"));

                    return View(transDetail);
                }
                else
                {
                    ViewBag.data = _context.GetTransactionDetails1(Convert.ToInt32(userId), yearvalue);

                    var transDetail = _context.GetTransactionDetails(Convert.ToInt32(userId), yearvalue);
                    ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
                    ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
                    return View(transDetail);
                }
            }
            catch (Exception ex)
            {

                TempData["ErrorMessage"] = $"Something went wrong: {ex.Message}";
                return View();
            }



            //int idint;
            //if (int.TryParse(id, out idint))
            //{
            //    try
            //    {
            //        int userId = Convert.ToInt32(_context.GetIdByNLK(id));
            //        ViewBag.data = _context.GetTransactionDetails1(Convert.ToInt32(userId), YearValue);

            //        var transDetail = _context.GetTransactionDetails(Convert.ToInt32(userId), YearValue);
            //        ViewBag.Sum = transDetail.Transactions.Sum(x => Convert.ToDecimal(x.Amount));
            //        ViewBag.AccountSum = transDetail.Accounts.Sum(x => x.Deposit);
            //        return View(transDetail);
            //    }
            //    catch (Exception Ex)
            //    {
            //        ModelState.AddModelError("", Ex.Message);
            //        return View();
            //    }
            //}
            //else
            //{
            //    ModelState.AddModelError("", "The NLK ID for this user is not valid!");
            //    return View();
            //}
        }

        [Route("admin/unverifiedoffices")]
        public ActionResult UnverifiedOffices()
        {

            ViewBag.Title = "Home" + " | " + "Unverified Office Detail";
            IEnumerable<Office> UnverifiedOfficeList = _context.GetUnverifiedOfficeList();
            return View(UnverifiedOfficeList);
        }
        [Route("admin/unverifiedoffice/{id}")]
        public ActionResult UnverifiedOfficeDetail(int id)
        {
            ViewBag.Title = "Home" + " | " + "Unverified Office Detail";
            Office unverifiedOffice = _context.GetOfficeDetailsById(id);
            return View(unverifiedOffice);
        }
        [Route("admin/verifyoffice/{id}")]
        public RedirectResult VerifyOffice(int id)
        {
            ViewBag.Title = "Home" + " | " + "Verify Office";
            _context.VerifyOffice(id);
            Office off = _context.GetOfficeDetailsById(id);
            StringBuilder builder = new StringBuilder();
            builder.Append("Your account at Nagarik Lagani Kosh with ID " + id + " has been Verified!");
            builder.AppendLine();
            builder.Append("Now You can login with the NLK ID and Password you provided at the time of Registration.");
            builder.AppendLine();
            builder.Append("In case you forgot your password, you can always reset your password at");
            builder.AppendLine();
            builder.Append("http://202.70.86.25/auth/corporate/forgotpassword");
            builder.AppendLine();
            builder.Append("NAGARIK LAGANI KOSH");
            builder.AppendLine();
            builder.Append("Baneshwor, Kathmandu");

            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("eservices.nlk@gmail.com", "eservice@cit1"),
                EnableSsl = true
            };
            client.Send("eservices.nlk@gmail.com", off.Email,
                "Account activated at Nagarik Lagani Kosh", builder.ToString());
            return Redirect("/admin/unverifiedoffices");
        }
        [Route("admin/rejectoffice/{id}")]
        public RedirectResult RejectOffice(int id, string message)
        {
            ViewBag.Title = "Home" + " | " + "Reject Office";
            Office off = _context.GetOfficeDetailsById(id);
            StringBuilder builder = new StringBuilder();
            builder.Append("Your application to register for e-services has been rejected.");
            builder.AppendLine();
            builder.Append(message);
            builder.AppendLine();
            builder.Append("Phone: 4781673 / 4785319 / 4785325 ");
            builder.AppendLine();
            builder.AppendLine();
            builder.Append("NAGARIK LAGANI KOSH");
            builder.AppendLine();
            builder.Append("Baneshwor, Kathmandu");

            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("eservices.nlk@gmail.com", "eservice@cit1"),
                EnableSsl = true
            };
            client.Send("eservices.nlk@gmail.com", off.Email,
                "Application for Registration Rejected", builder.ToString());
            Office office = _context.GetOfficeDetailsById(id);
            string path = Server.MapPath("~\\uploads\\officeapplications\\");
            if (System.IO.File.Exists(path + office.ApplicationLetterPath))
            {
                System.IO.File.Delete(path + office.ApplicationLetterPath);
            }
            _context.RejectOffice(id);
            return Redirect("/admin/unverifiedoffices");
        }
        public ActionResult VerifiedOffices()
        {
            ViewBag.Title = "Home" + " | " + "Verified Office Detail";
            IEnumerable<Office> UnverifiedOfficeList = _context.GetVerifiedOfficeList();
            return View(UnverifiedOfficeList);
        }
        [Route("admin/verifiedoffice/{id}")]
        public ActionResult VerifiedOfficeDetail(int id)
        {
            ViewBag.Title = "Home" + " | " + "Verified Office Detail";
            Office verifiedOffice = _context.GetOfficeDetailsById(id);
            VerifiedOfficeViewModel vovm = new VerifiedOfficeViewModel();
            vovm.Office = verifiedOffice;
            vovm.Staffs = _context.GetUsersByOfficeID(verifiedOffice.OfficeNLK.ToString());
            return View(vovm);
        }
        public ActionResult ChangePassword()
        {
            ViewBag.Title = "Home" + " | " + "Change Password";
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(AdminPasswordChangeViewModel model)
        {
            ViewBag.Title = "Home" + " | " + "Change Password";
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                if (model.NewPassword == model.NewPasswordConfirm)
                {
                    _context.AdminPasswordChange(User.Identity.Name, CreateMD5(model.NewPassword).ToLower());
                    ViewBag.Success = "The password has been changed!";
                    return View();
                }
                else
                {
                    ModelState.AddModelError("", "The passwords do not match!");
                    return View();
                }
            }
            catch (Exception Ex)
            {
                ViewBag.Error = Ex.Message;
                return View();
            }

        }
        private static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }
    }
}