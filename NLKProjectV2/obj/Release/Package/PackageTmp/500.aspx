﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="500.aspx.cs" Inherits="NLKProjectV2._500" %>

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Nagarik Lagani Kosh</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/font-awesome.min.css" rel="stylesheet" />
    <link href="Content/style.css" rel="stylesheet" />
    <link href="Content/menu.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 nogutter">
                <div class="site-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-1 col-md-1">
                                <img src="Images/cit_logo.png" class="img-responsive" />
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h1 class="nogutter">नागरिक लगानी कोष, नेपाल</h1>
                                <h3 class="nogutter">Citizen Investment Trust</h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="main-container">
                        <h1>Oops! There was some error. Please try again later.</h1>
                        <a runat="server" id="link" href="#"><i class="fa fa-home"></i> Go HOME</a>
                    </div>
                </div>
            </div>

            <footer class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="footer-content">
                        <p>&copy; Nagarik Lagani Kosh - @DateTime.Now.Year</p>
                        <p>Developed By: <a class="white-text" href="http://www.youngminds.com.np" target="_blank">Young Minds</a></p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>

